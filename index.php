<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$object = new Animal("shaun");

echo "Name  : " . $object->name . "<br>";
echo "Legs  : " . $object->legs . "<br>";
echo "Cold Blooded  : " . $object->cold_blooded . "<br><br>";


$object_frog = new Frog("buduk");

echo "Name  : " . $object_frog->name . "<br>";
echo "Legs  : " . $object_frog->legs . "<br>";
echo "Cold Blooded  : " . $object_frog->cold_blooded . "<br>";
echo "Jump  : " . $object_frog->jump . "<br><br>";


$object_ape = new Ape("kera sakti");

echo "Name  : " . $object_ape->name . "<br>";
echo "Legs  : " . $object_ape->ape_legs . "<br>";
echo "Cold Blooded  : " . $object_ape->cold_blooded . "<br>";
echo "Jump  : " . $object_ape->yell . "<br>";

?>